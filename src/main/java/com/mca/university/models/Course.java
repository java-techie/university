package com.mca.university.models;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
/**
 * To represent course entity
 * 
 * @author anghan
 *
 */
public class Course {

	private Integer id;

	private String name;

	private Integer deptId;

	private Integer credits;

	public Course() {

	}

	public Course(Integer id, String name, Integer deptId, Integer credits) {
		this.id = id;
		this.name = name;
		this.deptId = deptId;
		this.credits = credits;
	}
}
