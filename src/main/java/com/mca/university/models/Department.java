package com.mca.university.models;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
/**
 * To represent department entity
 * 
 * @author anghan
 *
 */
public class Department {

	public Department() {

	}

	public Department(Integer id, String name) {
		this.id = id;
		this.name = name;
	}

	private Integer id;

	private String name;

}
