package com.mca.university.models;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
/**
 * To represent professor entity
 * 
 * @author anghan
 *
 */
public class Professor {

	private Integer id;

	private String name;

	private Integer deptId;

	public Professor() {

	}

	public Professor(Integer id, String name, Integer deptId) {
		this.id = id;
		this.name = name;
		this.deptId = deptId;
	}
}
