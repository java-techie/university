package com.mca.university.models;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
/**
 * To represent schedule entity
 * 
 * @author anghan
 *
 */
public class Schedule {

	private Integer profId;

	private Integer courseId;

	private Integer semester;

	private Integer deptId;

	public Schedule() {

	}

	public Schedule(Integer profId, Integer courseId, Integer semester, Integer deptId) {
		this.profId = profId;
		this.courseId = courseId;
		this.semester = semester;
		this.deptId = deptId;
	}
}
