package com.mca.university.config;

import java.sql.Connection;
import java.sql.SQLException;

import com.mca.university.constants.AppConstants;
import com.zaxxer.hikari.HikariConfig;
import com.zaxxer.hikari.HikariDataSource;

public class DataSource {

	private static HikariConfig config = new HikariConfig();

	private static HikariDataSource ds;

	static {
		config.setDriverClassName(ConfigLoader.getProperties(AppConstants.DATASOURCE_DRIVER_CLASS_KEY));
		config.setJdbcUrl(ConfigLoader.getProperties(AppConstants.DATASOURCE_CONNECTION_URL_KEY));
		config.setUsername(ConfigLoader.getProperties(AppConstants.DATASOURCE_USERNAME_KEY));
		config.setPassword(ConfigLoader.getProperties(AppConstants.DATASOURCE_PASSWORD_KEY));
		config.setConnectionTimeout(
				Long.valueOf(ConfigLoader.getProperties(AppConstants.HIKARI_CONNECTION_POOL_TIMEOUT_KEY)));
		config.setMaximumPoolSize(
				Integer.valueOf(ConfigLoader.getProperties(AppConstants.HIKARI_CONNECTION_POOL_SIZE_KEY)));
		config.setMinimumIdle(
				Integer.valueOf(ConfigLoader.getProperties(AppConstants.HIKARI_CONNECTION_POOL_MINIMUM_IDLE_KEY)));
		config.setIdleTimeout(
				Long.valueOf(ConfigLoader.getProperties(AppConstants.HIKARI_CONNECTION_POOL_IDLE_TIMEOUT_KEY)));
		ds = new HikariDataSource(config);
	}

	private DataSource() {
	}

	public static Connection getConnection() throws SQLException {
		return ds.getConnection();
	}
}
