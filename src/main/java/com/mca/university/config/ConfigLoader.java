package com.mca.university.config;

import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

public class ConfigLoader {

	private static Properties config;
	private static String env;
	private static InputStream input;
	private static ClassLoader classLoader;

	/**
	 * This method will load the corresponding properties file based on the env and
	 * help to read property values
	 * 
	 * @author bipina
	 *
	 */
	private static void init() throws IOException {
		env = "dev";// System.getenv(BatchAttributes.ENV.toString()).toLowerCase();
		if (env == null || env.trim().isEmpty())
			throw new RuntimeException("Env value is null/blank ENV : " + env);

		config = new Properties();
		classLoader = new ConfigLoader().getClass().getClassLoader();
		input = classLoader.getResourceAsStream("application-" + env + ".properties");
		config.load(input);
	}

	/**
	 * This method will return an instance of properties. This will call the init
	 * method only if properties instance(config) making it singleton
	 * 
	 * @author bipina
	 *
	 */
	public static Properties getInstance() throws IOException {
		if (null == config) {
			init();
		}
		return config;

	}

	/**
	 * Method is used to get Properties from properties file as per env.
	 * 
	 * @param key
	 * @return
	 */
	public static String getProperties(String key) {
		try {
			return getInstance().getProperty(key);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}
}
