package com.mca.university.repositories;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

import org.springframework.stereotype.Repository;

import com.mca.university.config.DataSource;
import com.mca.university.models.Course;

import lombok.extern.slf4j.Slf4j;

@Repository
@Slf4j
public class CourseRepository {

	/**
	 * update course
	 * 
	 * @param dept
	 * @return
	 */
	public boolean update(Course dept) {
		String updateDepartment = "update \"university\".course set name=?, department_id=?, credits=? where id=? ";
		boolean isSuccess = false;
		try (Connection conn = DataSource.getConnection();
				PreparedStatement statement = conn.prepareStatement(updateDepartment)) {
			statement.setString(1, dept.getName());
			statement.setInt(2, dept.getDeptId());
			statement.setInt(3, dept.getCredits());
			statement.setInt(4, dept.getId());
			if (statement.executeUpdate() > 0) {
				log.info("Course {} updated successfully.", dept.getName());
				isSuccess = true;
			} else {
				log.info("Course {} update fail.", dept.getName());
			}
		} catch (Exception ex) {
			if (ex.getMessage().contains("duplicate key value violates unique constraint ")) {
				log.error("Error in save as duplicate Course not allowed", ex);
			} else {
				log.error("Error in update Course ", ex);
			}
		}
		return isSuccess;
	}

	/**
	 * save course
	 * 
	 * @param dept
	 * @return
	 */
	public boolean save(Course dept) {
		String insertDepartment = "insert into \"university\".course(name, department_id, credits) values(?, ?, ?)";
		boolean isSuccess = false;
		try (Connection conn = DataSource.getConnection();
				PreparedStatement statement = conn.prepareStatement(insertDepartment)) {
			statement.setString(1, dept.getName());
			statement.setInt(2, dept.getDeptId());
			statement.setInt(3, dept.getCredits());
			if (statement.executeUpdate() > 0) {
				log.info("Course {} inserted successfully.", dept.getName());
				isSuccess = true;
			} else {
				log.info("Course {} not able to inserted.", dept.getName());
			}
		} catch (Exception ex) {
			if (ex.getMessage().contains("duplicate key value violates unique constraint ")) {
				log.error("Error in save as duplicate Course not allowed", ex);
			} else {
				log.error("Error in save Course ", ex);
			}
		}
		return isSuccess;
	}

	/**
	 * get all course
	 * 
	 * @return
	 */
	public List<Course> findAll() {
		String selectDepartment = "select id, name, department_id, credits from \"university\".course";
		List<Course> courseList = null;
		try (Connection conn = DataSource.getConnection();
				PreparedStatement statement = conn.prepareStatement(selectDepartment);
				ResultSet resultSet = statement.executeQuery()) {
			courseList = new ArrayList<>();
			if (resultSet != null) {
				while (resultSet.next()) {
					Integer id = Integer.valueOf(resultSet.getString(1).toString());
					String name = resultSet.getString(2).toString();
					Integer dept_id = Integer.valueOf(resultSet.getString(3).toString());
					Integer credits = Integer.valueOf(resultSet.getString(4).toString());
					courseList.add(new Course(id, name, dept_id, credits));
				}
			}
		} catch (Exception ex) {
			log.error("Error in save findAll ", ex);
			throw new RuntimeException("Error in findAll while executing query : " + selectDepartment, ex);
		}
		return courseList;
	}

	public Course findById(Integer deptId) {
		String selectDepartment = "select id, name, department_id, credits from \"university\".course where id = ?";
		Course dept = null;
		try (Connection conn = DataSource.getConnection();
				PreparedStatement statement = conn.prepareStatement(selectDepartment)) {
			statement.setInt(1, deptId);
			ResultSet resultSet = statement.executeQuery();
			if (resultSet != null) {
				while (resultSet.next()) {
					Integer id = Integer.valueOf(resultSet.getString(1).toString());
					String name = resultSet.getString(2).toString();
					Integer dept_id = Integer.valueOf(resultSet.getString(3).toString());
					Integer credits = Integer.valueOf(resultSet.getString(4).toString());
					dept = new Course(id, name, dept_id, credits);
				}
			}
			if (resultSet != null)
				resultSet.close();
		} catch (Exception ex) {
			log.error("Error in findById ", ex);
			throw new RuntimeException("Error in findById while executing query : " + selectDepartment, ex);
		}
		return dept;
	}

	/**
	 * delete course by id
	 * 
	 * @param deptId
	 * @return
	 */
	public boolean deleteById(Integer deptId) {
		String deleteCourse = "delete from \"university\".course where id = ?";
		boolean isSuccess = false;
		try (Connection conn = DataSource.getConnection();
				PreparedStatement statement = conn.prepareStatement(deleteCourse)) {
			statement.setInt(1, deptId);
			if (statement.executeUpdate() > 0) {
				log.info("Course {} deleted successfully.", deptId);
				isSuccess = true;
			} else {
				log.info("Course {} not able to delete.", deptId);
			}
		} catch (Exception ex) {
			log.error("Error in deleteById department ", ex);
		}
		return isSuccess;
	}

}
