package com.mca.university.repositories;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.springframework.stereotype.Repository;

import com.mca.university.config.DataSource;
import com.mca.university.dto.ProfessorDto;
import com.mca.university.dto.ResultSetDto;
import com.mca.university.models.Professor;

import lombok.extern.slf4j.Slf4j;

@Repository
@Slf4j
public class ProfessorRepository {

	/**
	 * search professor with courses which has beens scheduled
	 * 
	 * @return
	 */
	public ResultSetDto<ProfessorDto> search() {
		ResultSetDto<ProfessorDto> result = null;
		String searchQuery = "select prof.name , STRING_AGG(distinct cs.name , ',')from \"university\".professor prof inner join \"university\".schedule sched on sched.professor_id  = prof.id\n"
				+ "inner join \"university\".course cs on cs.id  = sched.course_id group by prof.name ";
		try (Connection conn = DataSource.getConnection();
				PreparedStatement statement = conn.prepareStatement(searchQuery);
				ResultSet resultSet = statement.executeQuery()) {
			if (resultSet != null) {
				List<ProfessorDto> profList = new ArrayList<>();
				while (resultSet.next()) {
					String profName = resultSet.getString(1).toString();
					String courses = resultSet.getString(2).toString();
					profList.add(new ProfessorDto(profName, Arrays.asList(courses.split(","))));
				}
				result = new ResultSetDto<>();
				result.setResult(profList);
			}
		} catch (Exception ex) {
			log.error("Error in search ", ex);
			throw new RuntimeException("Error in search while executing query : " + searchQuery, ex);
		}
		return result;
	}

	/**
	 * update professor name and it's department by id
	 * 
	 * @param dept
	 * @return
	 */
	public boolean update(Professor dept) {
		String updateDepartment = "update \"university\".professor set name=?, department_id=? where id=? ";
		boolean isSuccess = false;
		try (Connection conn = DataSource.getConnection();
				PreparedStatement statement = conn.prepareStatement(updateDepartment)) {
			statement.setString(1, dept.getName());
			statement.setInt(2, dept.getDeptId());
			statement.setInt(3, dept.getId());
			if (statement.executeUpdate() > 0) {
				log.info("Professor {} updated successfully.", dept.getName());
				isSuccess = true;
			} else {
				log.info("Professor {} update fail.", dept.getName());
			}
		} catch (Exception ex) {
			if (ex.getMessage().contains("duplicate key value violates unique constraint ")) {
				log.error("Error in save as duplicate Professor not allowed", ex);
			} else {
				log.error("Error in update Professor ", ex);
			}
		}
		return isSuccess;
	}

	/**
	 * save new professor
	 * 
	 * @param dept
	 * @return
	 */
	public boolean save(Professor dept) {
		String insertDepartment = "insert into \"university\".professor(name, department_id) values(?, ?)";
		boolean isSuccess = false;
		try (Connection conn = DataSource.getConnection();
				PreparedStatement statement = conn.prepareStatement(insertDepartment)) {
			statement.setString(1, dept.getName());
			statement.setInt(2, dept.getDeptId());
			if (statement.executeUpdate() > 0) {
				log.info("Professor {} inserted successfully.", dept.getName());
				isSuccess = true;
			} else {
				log.info("Professor {} not able to inserted.", dept.getName());
			}
		} catch (Exception ex) {
			if (ex.getMessage().contains("duplicate key value violates unique constraint ")) {
				log.error("Error in save as duplicate Professor not allowed", ex);
			} else {
				log.error("Error in save Professor ", ex);
			}
		}
		return isSuccess;
	}

	/**
	 * get all professor
	 * 
	 * @return
	 */
	public List<Professor> findAll() {
		String selectDepartment = "select id, name, department_id from \"university\".professor";
		List<Professor> departmentList = null;
		try (Connection conn = DataSource.getConnection();
				PreparedStatement statement = conn.prepareStatement(selectDepartment);
				ResultSet resultSet = statement.executeQuery()) {
			departmentList = new ArrayList<>();
			if (resultSet != null) {
				while (resultSet.next()) {
					Integer id = Integer.valueOf(resultSet.getString(1).toString());
					String name = resultSet.getString(2).toString();
					Integer dept_id = Integer.valueOf(resultSet.getString(3).toString());
					departmentList.add(new Professor(id, name, dept_id));
				}
			}
		} catch (Exception ex) {
			log.error("Error in save findAll ", ex);
			throw new RuntimeException("Error in findAll while executing query : " + selectDepartment, ex);
		}
		return departmentList;
	}

	/**
	 * find professor by id
	 * 
	 * @param deptId
	 * @return
	 */
	public Professor findById(Integer deptId) {
		String selectDepartment = "select id, name, department_id from \"university\".professor where id = ?";
		Professor dept = null;
		try (Connection conn = DataSource.getConnection();
				PreparedStatement statement = conn.prepareStatement(selectDepartment)) {
			statement.setInt(1, deptId);
			ResultSet resultSet = statement.executeQuery();
			if (resultSet != null) {
				while (resultSet.next()) {
					Integer id = Integer.valueOf(resultSet.getString(1).toString());
					String name = resultSet.getString(2).toString();
					Integer dept_id = Integer.valueOf(resultSet.getString(3).toString());
					dept = new Professor(id, name, dept_id);
				}
			}
			if (resultSet != null)
				resultSet.close();
		} catch (Exception ex) {
			log.error("Error in findById ", ex);
			throw new RuntimeException("Error in findById while executing query : " + selectDepartment, ex);
		}
		return dept;
	}

	public boolean deleteById(Integer deptId) {
		String deleteDepartment = "delete from \"university\".professor where id = ?";
		boolean isSuccess = false;
		try (Connection conn = DataSource.getConnection();
				PreparedStatement statement = conn.prepareStatement(deleteDepartment)) {
			statement.setInt(1, deptId);
			if (statement.executeUpdate() > 0) {
				log.info("Professor {} deleted successfully.", deptId);
				isSuccess = true;
			} else {
				log.info("Professor {} not able to delete.", deptId);
			}
		} catch (Exception ex) {
			log.error("Error in deleteById department ", ex);
		}
		return isSuccess;
	}

}
