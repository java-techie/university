package com.mca.university.repositories;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

import org.springframework.stereotype.Repository;

import com.mca.university.config.DataSource;
import com.mca.university.models.Schedule;

import lombok.extern.slf4j.Slf4j;

@Repository
@Slf4j
public class ScheduleRepository {

	/**
	 * save schedule information base on department and course
	 * 
	 * @param dept
	 * @return
	 */
	public boolean save(Schedule dept) {
		String insertDepartment = "insert into \"university\".schedule(professor_id, course_id, semester, department_id) values(?, ?, ?, ?)";
		boolean isSuccess = false;
		try (Connection conn = DataSource.getConnection();
				PreparedStatement statement = conn.prepareStatement(insertDepartment)) {
			statement.setInt(1, dept.getProfId());
			statement.setInt(2, dept.getCourseId());
			statement.setInt(3, dept.getSemester());
			statement.setInt(4, dept.getDeptId());
			if (statement.executeUpdate() > 0) {
				log.info("Schedule inserted successfully.");
				isSuccess = true;
			} else {
				log.info("Schedule not able to inserted.");
			}
		} catch (Exception ex) {
			log.error("Error in save Schedule ", ex);
		}
		return isSuccess;
	}

	/**
	 * find all schedule entry
	 * 
	 * @return
	 */
	public List<Schedule> findAll() {
		String selectSchedule = "select professor_id, course_id, semester, department_id from \"university\".schedule";
		List<Schedule> scheduleList = null;
		try (Connection conn = DataSource.getConnection();
				PreparedStatement statement = conn.prepareStatement(selectSchedule);
				ResultSet resultSet = statement.executeQuery()) {
			scheduleList = new ArrayList<>();
			if (resultSet != null) {
				while (resultSet.next()) {
					Integer profId = Integer.valueOf(resultSet.getString(1).toString());
					Integer courseId = Integer.valueOf(resultSet.getString(2).toString());
					Integer semester = Integer.valueOf(resultSet.getString(3).toString());
					Integer deptId = Integer.valueOf(resultSet.getString(4).toString());
					scheduleList.add(new Schedule(profId, courseId, semester, deptId));
				}
			}
		} catch (Exception ex) {
			log.error("Error in findAll ", ex);
			throw new RuntimeException("Error in findAll while executing query : " + selectSchedule, ex);
		}
		return scheduleList;
	}

}
