package com.mca.university.repositories;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

import org.springframework.stereotype.Repository;

import com.mca.university.config.DataSource;
import com.mca.university.models.Department;

import lombok.extern.slf4j.Slf4j;

@Repository
@Slf4j
public class DepartmentRepository {

	/**
	 * update department name
	 * 
	 * @param dept
	 * @return
	 */
	public boolean update(Department dept) {
		String updateDepartment = "update \"university\".department set name=? where id=? ";
		boolean isSuccess = false;
		try (Connection conn = DataSource.getConnection();
				PreparedStatement statement = conn.prepareStatement(updateDepartment)) {
			statement.setString(1, dept.getName());
			statement.setInt(2, dept.getId());
			if (statement.executeUpdate() > 0) {
				log.info("Department {} updated successfully. ", dept.getName());
				isSuccess = true;
			} else {
				log.info("Department {} update fail.", dept.getName());
			}
		} catch (Exception ex) {
			if (ex.getMessage().contains("duplicate key value violates unique constraint ")) {
				log.error("Error in save as duplicate department not allowed", ex);
			} else {
				log.error("Error in update department ", ex);
			}
		}
		return isSuccess;
	}

	/**
	 * add new department
	 * 
	 * @param dept
	 * @return
	 */
	public boolean save(Department dept) {
		String insertDepartment = "insert into \"university\".department(name) values(?)";
		boolean isSuccess = false;
		try (Connection conn = DataSource.getConnection();
				PreparedStatement statement = conn.prepareStatement(insertDepartment)) {
			statement.setString(1, dept.getName());
			if (statement.executeUpdate() > 0) {
				log.info("Department {} inserted successfully.", dept.getName());
				isSuccess = true;
			} else {
				log.info("Department {} not able to inserted.", dept.getName());
			}
		} catch (Exception ex) {
			if (ex.getMessage().contains("duplicate key value violates unique constraint ")) {
				log.error("Error in save as duplicate department not allowed", ex);
			} else {
				log.error("Error in save department ", ex);
			}
		}
		return isSuccess;
	}

	/**
	 * get all department
	 * 
	 * @return
	 */
	public List<Department> findAll() {
		String selectDepartment = "select id, name from \"university\".department";
		List<Department> departmentList = null;
		try (Connection conn = DataSource.getConnection();
				PreparedStatement statement = conn.prepareStatement(selectDepartment);
				ResultSet resultSet = statement.executeQuery()) {
			departmentList = new ArrayList<>();
			if (resultSet != null) {
				while (resultSet.next()) {
					Integer id = Integer.valueOf(resultSet.getString(1).toString());
					String name = resultSet.getString(2).toString();
					departmentList.add(new Department(id, name));
				}
			}
		} catch (Exception ex) {
			log.error("Error in save findAll ", ex);
			throw new RuntimeException("Error in findAll while executing query : " + selectDepartment, ex);
		}
		return departmentList;
	}

	/**
	 * get department by id
	 * 
	 * @param deptId
	 * @return
	 */
	public Department findById(Integer deptId) {
		String selectDepartment = "select id, name from \"university\".department where id = ?";
		Department dept = null;
		try (Connection conn = DataSource.getConnection();
				PreparedStatement statement = conn.prepareStatement(selectDepartment)) {
			statement.setInt(1, deptId);
			ResultSet resultSet = statement.executeQuery();
			if (resultSet != null) {
				while (resultSet.next()) {
					Integer id = Integer.valueOf(resultSet.getString(1).toString());
					String name = resultSet.getString(2).toString();
					dept = new Department(id, name);
				}
			}
			if (resultSet != null)
				resultSet.close();
		} catch (Exception ex) {
			log.error("Error in findById ", ex);
			throw new RuntimeException("Error in findById while executing query : " + selectDepartment, ex);
		}
		return dept;
	}

	/**
	 * delete department by id
	 * 
	 * @param deptId
	 * @return
	 */
	public boolean deleteById(Integer deptId) {
		String deleteDepartment = "delete from \"university\".department where id = ?";
		boolean isSuccess = false;
		try (Connection conn = DataSource.getConnection();
				PreparedStatement statement = conn.prepareStatement(deleteDepartment)) {
			statement.setInt(1, deptId);
			if (statement.executeUpdate() > 0) {
				log.info("Department {} deleted successfully.", deptId);
				isSuccess = true;
			} else {
				log.info("Department {} not able to delete.", deptId);
			}
		} catch (Exception ex) {
			log.error("Error in deleteById department ", ex);
		}
		return isSuccess;
	}

}
