package com.mca.university.constants;

/**
 * class is used to define all constant for Univesity application
 * 
 * @author anghan
 *
 */
public class AppConstants {
	public static final String ERROR_CODE = "errorCode";
	public static final String ERROR_DESC = "errorDesc";

	// Hikari connection pool key
	public static final String HIKARI_CONNECTION_POOL_TIMEOUT_KEY = "spring.datasource.hikari.connection-timeout";
	public static final String HIKARI_CONNECTION_POOL_SIZE_KEY = "spring.datasource.hikari.maximum-pool-size";
	public static final String HIKARI_CONNECTION_POOL_MINIMUM_IDLE_KEY = "spring.datasource.hikari.minimum-idle";
	public static final String HIKARI_CONNECTION_POOL_IDLE_TIMEOUT_KEY = "spring.datasource.hikari.idle-timeout";

	public static final String DATASOURCE_DRIVER_CLASS_KEY = "spring.datasource.driver-class-name";
	public static final String DATASOURCE_CONNECTION_URL_KEY = "spring.datasource.url";
	public static final String DATASOURCE_USERNAME_KEY = "spring.datasource.username";
	public static final String DATASOURCE_PASSWORD_KEY = "spring.datasource.password";

	public static final int RECORD_SEARCH_LIMIT = 10;

}