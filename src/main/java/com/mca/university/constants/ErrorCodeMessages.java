package com.mca.university.constants;

/**
 * class is used to define all error code and messages for API RESPONSE.
 * 
 * @author anghan
 *
 */
public enum ErrorCodeMessages {

	UNIV_API_DATA_NOT_FOUND("UNIV_API_DATA_NOT_FOUND", "record not found.");

	private ErrorCodeMessages(String errMessage) {
		this.errorMessage = errMessage;
	}

	private ErrorCodeMessages(String errCode, String errMessage) {
		this.errorCode = errCode;
		this.errorMessage = errMessage;
	}

	String errorCode;

	String errorMessage;

	public String getErrorMessage() {
		return errorMessage;
	}

	public void setErrorMessage(String errorMessage) {
		this.errorMessage = errorMessage;
	}

	public String getErrorCode() {
		return errorCode;
	}

	public void setErrorCode(String errorCode) {
		this.errorCode = errorCode;
	}

	@Override
	public String toString() {
		return errorCode + "," + errorMessage;
	}
}
