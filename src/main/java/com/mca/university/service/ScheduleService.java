package com.mca.university.service;

import java.util.List;

import com.mca.university.models.Schedule;

/**
 * Schedule service for CRUD operation
 * 
 * @author anghan
 *
 */
public interface ScheduleService {
	// save operation
	/**
	 * To save schedule
	 * 
	 * @param schedule
	 * @return
	 */
	boolean saveSchedule(Schedule schedule);

	// read operation
	/**
	 * To fetch all schedule data
	 * 
	 * @return
	 */
	List<Schedule> fetchScheduleList();

}
