package com.mca.university.service;

import java.util.List;

import com.mca.university.models.Course;

/**
 * Course service for CRUD operation
 * 
 * @author anghan
 *
 */
public interface CourseService {
	/**
	 * To save course
	 * 
	 * @param course
	 * @return
	 */
	boolean saveCourse(Course course);

	/**
	 * To fetch all course
	 * 
	 * @return
	 */
	List<Course> fetchCourseList();

	/**
	 * To update course
	 * 
	 * @param course
	 * @return
	 */
	boolean updateCourse(Course course);

	/**
	 * To delete course by id
	 * 
	 * @param courseId
	 * @return
	 */
	boolean deleteCourseById(Integer courseId);

	/**
	 * To fetch course by id
	 * 
	 * @param courseId
	 * @return
	 */
	Course fetchCourseById(Integer courseId);
}
