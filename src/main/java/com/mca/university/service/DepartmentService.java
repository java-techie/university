package com.mca.university.service;

import java.util.List;

import com.mca.university.models.Department;

/**
 * Department service for CRUD operation
 * 
 * @author anghan
 *
 */
public interface DepartmentService {
	// save operation
	/**
	 * To save department
	 * 
	 * @param department
	 * @return
	 */
	boolean saveDepartment(Department department);

	// read operation
	/**
	 * To fetch all department
	 * 
	 * @return
	 */
	List<Department> fetchDepartmentList();

	// update operation
	/**
	 * To update department
	 * 
	 * @param department
	 * @return
	 */
	boolean updateDepartment(Department department);

	// delete operation
	/**
	 * To delete department by id
	 * 
	 * @param departmentId
	 * @return
	 */
	boolean deleteDepartmentById(Integer departmentId);

	/**
	 * To fetch department by id
	 * 
	 * @param departmentId
	 * @return
	 */
	Department fetchDepartmentById(Integer departmentId);
}