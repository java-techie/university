package com.mca.university.service;

import java.util.List;

import com.mca.university.dto.ProfessorDto;
import com.mca.university.dto.ResultSetDto;
import com.mca.university.models.Professor;

/**
 * Professor service for CRUD operation
 * 
 * @author anghan
 *
 */
public interface ProfessorService {
	/**
	 * To save professor
	 * 
	 * @param professor
	 * @return
	 */
	boolean saveProfessor(Professor professor);

	// read operation
	/**
	 * To fetch all professor
	 * 
	 * @return
	 */
	List<Professor> fetchProfessorList();

	/**
	 * To update professor
	 * 
	 * @param professor
	 * @return
	 */
	boolean updateProfessor(Professor professor);

	/**
	 * To delete professor
	 * 
	 * @param professorId
	 * @return
	 */
	boolean deleteProfessorById(Integer professorId);

	/**
	 * To fetch professor by id
	 * 
	 * @param professorId
	 * @return
	 */
	Professor fetchProfessorById(Integer professorId);

	/**
	 * To search professor with its course
	 * 
	 * @return
	 */
	ResultSetDto<ProfessorDto> search();
}
