package com.mca.university.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.mca.university.models.Course;
import com.mca.university.repositories.CourseRepository;
import com.mca.university.service.CourseService;

@Service
public class CourseServiceImpl implements CourseService {

	@Autowired
	private CourseRepository courseRepository;

	// save operation
	@Override
	public boolean saveCourse(Course course) {
		return courseRepository.save(course);
	}

	// read operation
	@Override
	public List<Course> fetchCourseList() {
		return courseRepository.findAll();
	}

	// update operation
	@Override
	public boolean updateCourse(Course course) {
		return courseRepository.update(course);
	}

	// delete operation
	@Override
	public boolean deleteCourseById(Integer courseId) {
		return courseRepository.deleteById(courseId);
	}

	// fetch by id
	@Override
	public Course fetchCourseById(Integer courseId) {
		return courseRepository.findById(courseId);
	}

}
