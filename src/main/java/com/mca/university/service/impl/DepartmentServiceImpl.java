package com.mca.university.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.mca.university.models.Department;
import com.mca.university.repositories.DepartmentRepository;
import com.mca.university.service.DepartmentService;

@Service
public class DepartmentServiceImpl implements DepartmentService {

	@Autowired
	private DepartmentRepository departmentRepository;

	// save operation
	@Override
	public boolean saveDepartment(Department department) {
		return departmentRepository.save(department);
	}

	// read operation
	@Override
	public List<Department> fetchDepartmentList() {
		return departmentRepository.findAll();
	}

	// update operation
	@Override
	public boolean updateDepartment(Department department) {
		return departmentRepository.update(department);
	}

	// delete operation
	@Override
	public boolean deleteDepartmentById(Integer departmentId) {
		return departmentRepository.deleteById(departmentId);
	}

	// fetch by id
	@Override
	public Department fetchDepartmentById(Integer departmentId) {
		return departmentRepository.findById(departmentId);
	}

}
