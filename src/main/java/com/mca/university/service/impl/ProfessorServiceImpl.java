package com.mca.university.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.mca.university.dto.ProfessorDto;
import com.mca.university.dto.ResultSetDto;
import com.mca.university.models.Professor;
import com.mca.university.repositories.ProfessorRepository;
import com.mca.university.service.ProfessorService;

@Service
public class ProfessorServiceImpl implements ProfessorService {

	@Autowired
	private ProfessorRepository professorRepository;

	// save operation
	@Override
	public boolean saveProfessor(Professor Professor) {
		return professorRepository.save(Professor);
	}

	// read operation
	@Override
	public List<Professor> fetchProfessorList() {
		return professorRepository.findAll();
	}

	// update operation
	@Override
	public boolean updateProfessor(Professor professor) {
		return professorRepository.update(professor);
	}

	// delete operation
	@Override
	public boolean deleteProfessorById(Integer professorId) {
		return professorRepository.deleteById(professorId);
	}

	// fetch by id
	@Override
	public Professor fetchProfessorById(Integer professorId) {
		return professorRepository.findById(professorId);
	}

	@Override
	public ResultSetDto<ProfessorDto> search() {
		return professorRepository.search();
	}
}
