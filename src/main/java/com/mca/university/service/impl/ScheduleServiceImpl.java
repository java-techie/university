package com.mca.university.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.mca.university.models.Schedule;
import com.mca.university.repositories.ScheduleRepository;
import com.mca.university.service.ScheduleService;

@Service
public class ScheduleServiceImpl implements ScheduleService {

	@Autowired
	private ScheduleRepository scheduleSRepository;

	// save operation
	@Override
	public boolean saveSchedule(Schedule schedule) {
		return scheduleSRepository.save(schedule);
	}

	// read operation
	@Override
	public List<Schedule> fetchScheduleList() {
		return scheduleSRepository.findAll();
	}

}
