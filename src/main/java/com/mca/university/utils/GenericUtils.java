package com.mca.university.utils;

import java.util.HashMap;
import java.util.Map;

import com.mca.university.constants.AppConstants;
import com.mca.university.constants.ErrorCodeMessages;

public class GenericUtils {
	/**
	 * add error code and description from enum
	 * 
	 * @param map
	 * @param errorCodeMessage
	 * @return
	 */
	public static Map<String, Object> addErrorCodeMessage(Map<String, Object> map, ErrorCodeMessages errorCodeMessage) {
		if (map == null) {
			map = new HashMap<>();
		}
		map.put(AppConstants.ERROR_CODE, errorCodeMessage.getErrorCode());
		map.put(AppConstants.ERROR_DESC, errorCodeMessage.getErrorMessage());
		return map;
	}

}
