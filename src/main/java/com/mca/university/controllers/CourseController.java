package com.mca.university.controllers;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.mca.university.constants.ErrorCodeMessages;
import com.mca.university.models.Course;
import com.mca.university.service.CourseService;
import com.mca.university.utils.GenericUtils;

@RestController
public class CourseController {
	// Annotation
	@Autowired
	private CourseService courseService;

	// Save operation
	@PostMapping("/courses")
	public ResponseEntity<String> saveCourse(@RequestBody Course course) {
		if (courseService.saveCourse(course)) {
			return new ResponseEntity<>("Course Added Successfully...!", HttpStatus.OK);
		} else {
			return new ResponseEntity<>("Course Not Added...!", HttpStatus.CONFLICT);
		}

	}

	// Read operation
	@GetMapping("/courses")
	public ResponseEntity<List<Course>> fetchCourseList() {
		List<Course> result = courseService.fetchCourseList();
		return new ResponseEntity<>(result, HttpStatus.OK);
	}

	// Update operation
	@PutMapping("/courses/{id}")
	public ResponseEntity<String> updateCourse(@RequestBody Course course, @PathVariable("id") Integer courseId) {
		course.setId(courseId);
		if (courseService.updateCourse(course)) {
			return new ResponseEntity<>("Course Updated Successfully...!", HttpStatus.OK);
		} else {
			return new ResponseEntity<>("Course Not Updated...!", HttpStatus.NOT_FOUND);
		}
	}

	// Update operation
	@GetMapping("/courses/{id}")
	public ResponseEntity<Map<String, Object>> fetchCourseById(@PathVariable("id") Integer courseId) {
		Course dbCourse = courseService.fetchCourseById(courseId);
		Map<String, Object> response = null;
		if (dbCourse == null) {
			return new ResponseEntity<>(
					GenericUtils.addErrorCodeMessage(response, ErrorCodeMessages.UNIV_API_DATA_NOT_FOUND),
					HttpStatus.NOT_FOUND);
		} else {
			response = new HashMap<>(1);
			response.put("data", dbCourse);
			return new ResponseEntity<>(response, HttpStatus.OK);
		}
	}

	// Delete operation
	@DeleteMapping("/courses/{id}")
	public ResponseEntity<String> deleteCourseById(@PathVariable("id") Integer courseId) {
		if (courseService.deleteCourseById(courseId)) {
			return new ResponseEntity<>("Course Deleted Successfully...!", HttpStatus.OK);
		} else {
			return new ResponseEntity<>("Course Not Able Delete...!", HttpStatus.NOT_FOUND);
		}
	}
}
