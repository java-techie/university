package com.mca.university.controllers;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.mca.university.models.Schedule;
import com.mca.university.service.ScheduleService;

@RestController
public class ScheduleController {
	// Annotation
	@Autowired
	private ScheduleService professorService;

	// Save operation
	@PostMapping("/schedules")
	public ResponseEntity<String> saveSchedule(@RequestBody Schedule professor) {
		if (professorService.saveSchedule(professor)) {
			return new ResponseEntity<>("Schedule Added Successfully...!", HttpStatus.OK);
		} else {
			return new ResponseEntity<>("Schedule Not Added...!", HttpStatus.CONFLICT);
		}

	}

	// Read operation
	@GetMapping("/schedules")
	public ResponseEntity<List<Schedule>> fetchScheduleList() {
		List<Schedule> result = professorService.fetchScheduleList();
		return new ResponseEntity<>(result, HttpStatus.OK);
	}

}