package com.mca.university.controllers;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.mca.university.constants.ErrorCodeMessages;
import com.mca.university.models.Department;
import com.mca.university.service.DepartmentService;
import com.mca.university.utils.GenericUtils;

@RestController
public class DepartmentController {
	// Annotation
	@Autowired
	private DepartmentService departmentService;

	// Save operation
	@PostMapping("/departments")
	public ResponseEntity<String> saveDepartment(@RequestBody Department department) {
		if (departmentService.saveDepartment(department)) {
			return new ResponseEntity<>("Department Added Successfully...!", HttpStatus.OK);
		} else {
			return new ResponseEntity<>("Department Not Added...!", HttpStatus.CONFLICT);
		}

	}

	// Read operation
	@GetMapping("/departments")
	public ResponseEntity<List<Department>> fetchDepartmentList() {
		List<Department> result = departmentService.fetchDepartmentList();
		return new ResponseEntity<>(result, HttpStatus.OK);
	}

	// Update operation
	@PutMapping("/departments/{id}")
	public ResponseEntity<String> updateDepartment(@RequestBody Department department,
			@PathVariable("id") Integer departmentId) {
		department.setId(departmentId);
		if (departmentService.updateDepartment(department)) {
			return new ResponseEntity<>("Department Updated Successfully...!", HttpStatus.OK);
		} else {
			return new ResponseEntity<>("Department Not Updated...!", HttpStatus.NOT_FOUND);
		}
	}

	// Update operation
	@GetMapping("/departments/{id}")
	public ResponseEntity<Map<String, Object>> fetchDepartmentById(@PathVariable("id") Integer departmentId) {
		Department dbDept = departmentService.fetchDepartmentById(departmentId);
		Map<String, Object> response = null;
		if (dbDept == null) {
			return new ResponseEntity<>(
					GenericUtils.addErrorCodeMessage(response, ErrorCodeMessages.UNIV_API_DATA_NOT_FOUND),
					HttpStatus.NOT_FOUND);
		} else {
			response = new HashMap<>(1);
			response.put("data", dbDept);
			return new ResponseEntity<>(response, HttpStatus.OK);
		}
	}

	// Delete operation
	@DeleteMapping("/departments/{id}")
	public ResponseEntity<String> deleteDepartmentById(@PathVariable("id") Integer departmentId) {
		if (departmentService.deleteDepartmentById(departmentId)) {
			return new ResponseEntity<>("Department Deleted Successfully...!", HttpStatus.OK);
		} else {
			return new ResponseEntity<>("Department Not Able Delete...!", HttpStatus.NOT_FOUND);
		}
	}
}
