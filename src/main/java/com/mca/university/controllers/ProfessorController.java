package com.mca.university.controllers;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.mca.university.constants.ErrorCodeMessages;
import com.mca.university.dto.ProfessorDto;
import com.mca.university.dto.ResultSetDto;
import com.mca.university.models.Professor;
import com.mca.university.service.ProfessorService;
import com.mca.university.utils.GenericUtils;

@RestController
public class ProfessorController {
	// Annotation
	@Autowired
	private ProfessorService professorService;

	// Save operation
	@PostMapping("/professors")
	public ResponseEntity<String> saveProfessor(@RequestBody Professor professor) {
		if (professorService.saveProfessor(professor)) {
			return new ResponseEntity<>("Professor Added Successfully...!", HttpStatus.OK);
		} else {
			return new ResponseEntity<>("Professor Not Added...!", HttpStatus.CONFLICT);
		}

	}

	// Read operation
	@GetMapping("/professors")
	public ResponseEntity<List<Professor>> fetchProfessorList() {
		List<Professor> result = professorService.fetchProfessorList();
		return new ResponseEntity<>(result, HttpStatus.OK);
	}

	// Update operation
	@PutMapping("/professors/{id}")
	public ResponseEntity<String> updateProfessor(@RequestBody Professor professor,
			@PathVariable("id") Integer professorId) {
		professor.setId(professorId);
		if (professorService.updateProfessor(professor)) {
			return new ResponseEntity<>("Professor Updated Successfully...!", HttpStatus.OK);
		} else {
			return new ResponseEntity<>("Professor Not Updated...!", HttpStatus.NOT_FOUND);
		}
	}

	// Update operation
	@GetMapping("/professors/{id}")
	public ResponseEntity<Map<String, Object>> fetchProfessorById(@PathVariable("id") Integer professorId) {
		Professor dbProfessor = professorService.fetchProfessorById(professorId);
		Map<String, Object> response = null;
		if (dbProfessor == null) {
			return new ResponseEntity<>(
					GenericUtils.addErrorCodeMessage(response, ErrorCodeMessages.UNIV_API_DATA_NOT_FOUND),
					HttpStatus.NOT_FOUND);
		} else {
			response = new HashMap<>(1);
			response.put("data", dbProfessor);
			return new ResponseEntity<>(response, HttpStatus.OK);
		}
	}

	@GetMapping("/professors/search")
	public ResponseEntity<List<ProfessorDto>> search() {
		ResultSetDto<ProfessorDto> professorResult = professorService.search();
		if (professorResult != null && professorResult.getResult() != null && !professorResult.getResult().isEmpty()) {
			return new ResponseEntity<>(professorResult.getResult(), HttpStatus.OK);
		} else {
			return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);
		}
	}
}
