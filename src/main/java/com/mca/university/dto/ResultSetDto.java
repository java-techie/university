package com.mca.university.dto;

import java.util.List;

import com.mca.university.constants.AppConstants;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
/**
 * DTO is used to generic search result
 * 
 * @author anghan
 *
 * @param <T>
 */
public class ResultSetDto<T> {
	int limit = AppConstants.RECORD_SEARCH_LIMIT; // fetch record
	int offset = 0;
	List<T> result;
}
