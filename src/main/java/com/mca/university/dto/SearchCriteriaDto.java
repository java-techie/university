package com.mca.university.dto;

import java.util.List;

import com.mca.university.constants.AppConstants;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
/**
 * DTO is used to for generic search criteria
 * 
 * @author anghan
 *
 */
public class SearchCriteriaDto {
	private int limit = AppConstants.RECORD_SEARCH_LIMIT; // fetch record
	private int offset = 0;// starting position of search
	private List<FieldDto> fieldDto;

}
