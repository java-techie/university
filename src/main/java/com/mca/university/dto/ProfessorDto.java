package com.mca.university.dto;

import java.util.List;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
/**
 * DTO is used to represent professor search data
 * 
 * @author anghan
 *
 */
public class ProfessorDto {
	private String name;
	private List<String> courses;

	public ProfessorDto() {

	}

	public ProfessorDto(String name, List<String> courses) {
		this.name = name;
		this.courses = courses;
	}
}
