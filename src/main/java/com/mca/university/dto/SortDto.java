package com.mca.university.dto;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
/**
 * DTO is used to define sort field in search criteria
 * 
 * @author anghan
 *
 */
public class SortDto {
	private String fieldName;
	private ORDER order;

	public enum ORDER {
		ASC, DESC
	}

}
