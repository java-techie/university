package com.mca.university.dto;

/**
 * DTO is used to generic field define in search criteria
 * 
 * @author anghan
 *
 */
public class FieldDto {
	String name;
	String value;

}
