package com.mca.university;

import java.util.List;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import com.mca.university.dto.ProfessorDto;
import com.mca.university.dto.ResultSetDto;
import com.mca.university.service.ProfessorService;

@SpringBootTest
class ProfessorTests {

	@Autowired
	ProfessorService professorService;

	@Test
	public void searchProfessor() {
		System.out.println("########### Start Search #############");
		ResultSetDto<ProfessorDto> result = professorService.search();
		if (result.getResult() != null && !result.getResult().isEmpty()) {
			List<ProfessorDto> professorList = result.getResult();
			for (ProfessorDto prof : professorList) {
				System.out.println(prof.getName() + " : " + prof.getCourses());
			}
		}
		System.out.println("########### End Search #############");
	}
}