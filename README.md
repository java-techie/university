# University
A university maintains data on professors, departments, courses, and schedules in four tables:
department, professor, course, and schedule.

## Assumption
all db tables are already created.

Note : scahmeName: university


## Getting started


## tools
JDK 8 ,
Maven ,
Postgress DB

## Build
```
cd <any_folder>
git clone  https://gitlab.com/java-techie/university.git
mvn clean package
java -jar ./target/university-1.0.0-SNAPSHOT.jar
Open : http://localhost:8080/swagger-ui.html

```

## Proposed next steps/improvements:
```
1) global exception handling 
2) add validation part before insert/update (foreign key related issue or unwanted data push)
3) for search API should be dynamic so that we can pass parameter and base on that it should give output (that is why i have created SearchCriteriaDto)
4) As per enviroment wise we can put configuration file so that it can pick up env. wise configuration (same as Profiles)
5) we can use hibernate to remove query part and can ease. 
6) more junit can written for other component like department/professor/schedule/courses.
```
