drop table university.department;
drop table university.professor;
drop table university.schedule;   
drop table university.course;

CREATE TABLE IF NOT exists university.department (
	id SERIAL PRIMARY KEY,
	"name" varchar(255) NOT NULL,
	CONSTRAINT uk_name UNIQUE (name)
);

CREATE TABLE IF NOT exists university.professor (
	id SERIAL PRIMARY KEY ,
	department_id int4 NULL,
	"name" varchar(255) NULL
);

CREATE TABLE IF NOT exists university.schedule (
	professor_id int4 NULL,
	course_id int4 NULL,
	semester int4 NULL,
	department_id int4 NULL
);

CREATE TABLE IF NOT exists university.course (
	id SERIAL PRIMARY KEY ,
	credits int4 NULL,
	department_id int4 NULL,
	"name" varchar(255) NULL
);